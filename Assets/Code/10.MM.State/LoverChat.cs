﻿#nullable enable

using System.Collections.Generic;

namespace MM.State {
	public sealed class LoverChat {
		private readonly List<LoverChatEntry> entries = new();
		public IReadOnlyList<LoverChatEntry> Entries => entries;

		internal void Add(LoverChatEntry entry) {
			entries.Add(entry);
		}
	}

	public readonly struct LoverChatEntry {
		public readonly bool IsLover;
		public readonly string Text;

		public LoverChatEntry(bool isLover, string text) {
			IsLover = isLover;
			Text = text;
		}
	}
}
