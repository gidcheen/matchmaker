﻿#nullable enable

namespace MM.State {
	public readonly struct Question {
		public readonly string Text;
		public readonly string Answer;

		public Question(string text, string answer) {
			Text = text;
			Answer = answer;
		}
	}
}
