﻿#nullable enable

using System.Collections.Generic;

namespace MM.State {
	public readonly struct Matchup {
		public readonly Lover Lover1;
		public readonly Lover Lover2;

		public Matchup(Lover lover1, Lover lover2) {
			Lover1 = lover1;
			Lover2 = lover2;
		}

		public readonly IEnumerable<Lover> Lovers() {
			yield return Lover1;
			yield return Lover2;
		}

		public readonly MatchupScore Score() {
			if (Lover1.Name == Lover2.PrimaryLover && Lover2.Name == Lover1.PrimaryLover) {
				return MatchupScore.Good;
			} else {
				return MatchupScore.Medium;
			}
		}
	}

	public enum MatchupScore {
		Good,
		Medium,
		Bad,
	}
}
