﻿#nullable enable

using MM.Services;
using System.Collections.Generic;
using System.Linq;

namespace MM.State {
	public sealed class Game {
		private readonly Lover[] lovers;
		private readonly Dictionary<Lover, LoverChat> chats;
		private GameState state;
		private int round;

		public GameState State => state;

		public IReadOnlyList<Lover> Lovers => lovers;
		public IReadOnlyDictionary<Lover, LoverChat> Chats => chats;
		public int Round => round;

		public Game(Csv loverData) {
			//lovers = Lover.FromCsv(loverData);
			lovers = new Lover[] {
				new Lover(),
				new Lover(),
				new Lover(),
				new Lover(),
				new Lover(),
				new Lover(),
				new Lover(),
				new Lover(),
			};
			chats = lovers.ToDictionary(l => l, l => new LoverChat());
			state = new MatchupState(this);
		}

		internal void NextState(GameState newState) {
			state = newState;
		}
	}
}
