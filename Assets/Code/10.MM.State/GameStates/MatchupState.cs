﻿#nullable enable

using System.Collections.Generic;
using System.Linq;

namespace MM.State {
	public sealed class MatchupState : GameState {
		private Lover? selected = null;
		public readonly List<Matchup> Matchups = new();
		private readonly Dictionary<Lover, bool> loversAsked;

		internal MatchupState(Game game) : base(game) {
			loversAsked = game.Lovers.ToDictionary(l => l, l => false);
		}

		public void Match(Matchup matchup) {
			Matchups.Add(matchup);
			if (Matchups.Count >= Game.Lovers.Count / 2) {
				Game.NextState(new GameMatchupResult(Game));
			}
		}
	}
}
