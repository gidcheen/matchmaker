﻿#nullable enable

namespace MM.State {
	public sealed class GameFinished : GameState {
		internal GameFinished(Game game) : base(game) {
		}
	}
}
