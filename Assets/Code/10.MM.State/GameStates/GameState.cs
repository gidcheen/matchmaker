﻿#nullable enable

namespace MM.State {
	public abstract class GameState {
		public readonly Game Game;

		internal GameState(Game game) {
			Game = game;
		}
	}
}
