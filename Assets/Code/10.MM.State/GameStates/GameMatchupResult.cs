﻿#nullable enable

namespace MM.State {
	public sealed class GameMatchupResult : GameState {
		internal GameMatchupResult(Game game) : base(game) {
		}
	}
}
