﻿#nullable enable

using MM.Services;
using System;
using System.Collections.Generic;

namespace MM.State {
	public class Lover {
		public readonly string Name = "";
		public readonly string Age = "";
		public readonly string Gender = "";
		public readonly string Quote = "";

		public readonly IReadOnlyList<Question> Questions = new Question[0];

		public readonly string PrimaryLover = string.Empty;
		public readonly IReadOnlyList<string> SecondaryLovers = new string[0];

		public static Lover[] FromCsv(Csv csv) {
			return Array.Empty<Lover>();
		}
	}
}
