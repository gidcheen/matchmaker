﻿using MM.State;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace MM.View {
	public class ChatView : MonoBehaviour {
		[SerializeField]
		private Button[] optionButtons = null;

		[SerializeField]
		private Image loverPic;
		[SerializeField]
		private Text loverName;

		[SerializeField]
		private RectTransform messagesRoot = null;

		[SerializeField]
		private UiConfig config;

		private Lover lover;

		public void SetLover(Game game, Lover lover) {
			this.lover = lover;
			loverPic.sprite = lover != null ? config.PicOfLover(lover) : null;
			loverName.text = lover?.Name;

			while (messagesRoot.childCount != 0) {
				Destroy(messagesRoot.GetChild(0).gameObject);
			}
			if (lover != null) {
				var chat = game.Chats[lover];
				foreach (var entry in chat.Entries) {
					var prefab = entry.IsLover ? config.LoverMessagePrefab : config.PlayerMessagePrefab;
					var message = Instantiate(prefab, messagesRoot);
					message.SetMessge(entry.Text);
				}
				if (chat.Entries.Count(e => !e.IsLover) < game.Round) {
					foreach (var b in optionButtons) {
						b.gameObject.SetActive(false);
					}
				} else {
					foreach (var b in optionButtons) {
						b.gameObject.SetActive(false);
					}
				}
			}
		}
	}
}
