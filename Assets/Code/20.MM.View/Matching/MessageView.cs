﻿using UnityEngine;
using UnityEngine.UI;

namespace MM.View {
	internal class MessageView : MonoBehaviour {
		[SerializeField]
		private Text message = null;

		public void SetMessge(string message) {
			this.message.text = message;
			GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, this.message.preferredHeight);
		}
	}
}
