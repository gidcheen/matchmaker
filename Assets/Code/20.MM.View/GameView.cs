using UnityEngine;

namespace MM.View {
	public sealed class GameView : MonoBehaviour {
		[SerializeField]
		private TextAsset loverData = null;
		[SerializeField]
		private TextAsset loverMatchups = null;

		[SerializeField]
		private MatchupView matchupView = null;
		[SerializeField]
		private DatesView datesView = null;
		[SerializeField]
		private FinishView finishView = null;
	}
}
