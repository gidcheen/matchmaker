﻿#nullable enable

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MM.Services {
	public sealed class Csv {
		private readonly string[,] values;

		public int RowCount => values.GetLength(0);
		public int ColCount => values.GetLength(1);

		public Csv(string csvString) {
			values = csvString
				.Split('\n')
				.Select(r => r.Trim())
				.Where(r => r.Length > 0)
				.Select(SplitRow)
				.ToArray()
				.To2D();
		}

		public string this[int col, int row] => values[col, row];

		public static Csv Load(string path) {
			return new Csv(File.ReadAllText(path));
		}

		public string? ColElement(string col, int row) {
			if (row >= RowCount)
				return null;
			for (int i = 0; i < ColCount; i++) {
				if (values[0, i] == col) {
					return values[row, i];
				}
			}
			return null;
		}

		public IEnumerable<string> FindRow(string key) {
			if (ColCount == 0)
				yield break;
			for (int i = 0; i < RowCount; i++) {
				if (values[i, 0] == key) {
					for (int j = 0; j < ColCount; j++) {
						yield return values[i, j];
					}
				}
			}
		}

		public IEnumerable<string> FindCol(string key) {
			if (RowCount == 0)
				yield break;
			for (int i = 0; i < ColCount; i++) {
				if (values[0, i] == key) {
					for (int j = 0; j < RowCount; j++) {
						yield return values[j, i];
					}
				}
			}
		}

		private static string[] SplitRow(string rowString) {
			rowString = rowString.Trim();

			var elements = new List<StringBuilder> { new() };
			var inQuotes = false;
			foreach (var c in rowString) {
				switch (c) {
				case '"':
					inQuotes = !inQuotes;
					continue;
				case ',' when !inQuotes:
					elements.Add(new());
					continue;
				default:
					elements[^1].Append(c);
					break;
				}
			}

			return elements.Select(sb => sb.ToString().Trim()).ToArray();
		}
	}

	internal static class ArrayExt {
		internal static T[,] To2D<T>(this T[][] source) {
			var FirstDim = source.Length;
			var SecondDim = source.Min(s => s.Length);

			var result = new T[FirstDim, SecondDim];
			for (var i = 0; i < FirstDim; ++i)
				for (var j = 0; j < SecondDim; ++j)
					result[i, j] = source[i][j];

			return result;
		}
	}
}
